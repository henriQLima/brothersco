/*
* HL Global variables and options - move
*/

console.log('loaded app.js', 'v2.0')

// Declaring the variables used on the functions for this app.js
var map,
  Lotes,
  LotesData,
  LotesCenter,
  LotesSearch,
  roadsData,
  roadsTopojson,
  currentStyle,
  newStyle,
  searchResultCurrent,
  searchResultId,
  searchsearchResultString,
  LotesCentroids,
  popup,
  popup2,
  mapBasemaps = [],
  mapLayers = [],
  flying = false,
  baseLayerChange = false,
  searchFound = false,
  gmapsorig,
  userLoc = 0,
  tries = 0;

var options = {
}

var token = 'pk.eyJ1IjoiaGVucmlxbGltYSIsImEiOiI2NGVhNTllNTNjOWVmZGVlMDhiOGYzMWQ4MmE4NGY2OSJ9.TfjCYyxPQ_gNcd1JEL7C2w';


/*
* Wait for all data to download before building map
*/
// $.ajax({
//   type: "GET",
//   crossDomain: true,
//   dataType: 'jsonp',
//   headers: {"Access-Control-Allow-Origin": "https://www.dropbox.com/s/q4rplga2itryqqo/Status_Lotes_JDF-02-10.geojson?dl=0"},
//   url: "https://www.dropbox.com/s/q4rplga2itryqqo/Status_Lotes_JDF-02-10.geojson?dl=0"
// }).done(function (data) {
//   console.log('minha data', data);
// });

$.when(
  $.getJSON('https://dl.dropboxusercontent.com/s/nxia8spw2lw4onz/Status_Lotes_JDF-22-10.geojson?dl=0', function (loadeddata1) {
    LotesData = loadeddata1;
  })
).then(function () {
  mapInit();
});

/*
* Map functions
*/

function mapInit(div, opts, key) {
  console.log('building map');

  //Getting a centroid from each parcel on the map to point to the route on google maps
  LotesCenter = turf.center(LotesData);

  mapboxgl.accessToken = token;

  mapBasemaps.streets = 'mapbox://styles/mapbox/streets-v9';
  currentStyle = 'streets-v9';
  mapBasemaps.satellite = 'mapbox://styles/mapbox/satellite-streets-v9';

  addMapBasemapsToggle('basemapSwitcher', 'streets-v9', 'Streets', false, 1);
  addMapBasemapsToggle('basemapSwitcher', 'satellite-streets-v9', 'Satellite', true, 2);

  map = new mapboxgl.Map({
    container: 'map',
    //hash: true,
    style: mapBasemaps.satellite,
    //center: [LotesCenter.geometry.coordinates[0],LotesCenter.geometry.coordinates[1]],
    center: [-46.586032, -21.812202], 
    zoom: 16.5,
    maxZoom: 19.49,
    pitch: 0,
    preserveDrawingBuffer: true
  });

  map.addControl(new mapboxgl.NavigationControl());
  // var gpscontrol = new mapboxgl.GeolocateControl({
  //   positionOptions: {
  //     enableHighAccuracy: true
  //   },
  //   trackUserLocation: true
  // });

  // map.addControl(gpscontrol);
  // gpscontrol.on('geolocate', function (e) {
  //   userLoc = 1;
  //   gmapsorig = e.coords.latitude + "," + e.coords.longitude;
  // });

  map.addControl(new mapboxgl.FullscreenControl());

  //TESTING ROUTE TOOLS DIRECT FROM MAPBOX API 
  /*  var mbrouting = new MapboxDirections({
      accessToken: token
    });
    map.addControl(mbrouting, 'top-left')
  
    $("#mboxrouting").on('click', function() {
      if($(".mapboxgl-ctrl-directions").length === 0) {
        map.addControl(mbrouting, 'top-left')
      }else{
        map.removeControl(mbrouting);
        map.removeSource('directions');
      }
    })*/

  //POPUP CONFIG
  popup = new mapboxgl.Popup();
  popup2 = new mapboxgl.Popup();

  popup.on('close', function () {
    if (typeof map.getLayer('selected') != "undefined") {
      map.removeLayer('selected')
      //map.removeSource('selected');
    }
  });
  map.on('load', function () {
    addSources();
    searchInit();
  });
}

//Functions that links the menu and checkbox with the map parcel features
//1.
function addMapLayersToggle(id, layer, name, on) {
  if (!document.getElementById(layer)) {
    var label = document.createElement('label');
    var input = document.createElement('input');
    input.id = layer;
    input.type = 'checkbox';
    input.className = 'mdl-checkbox__input';
    var span = document.createElement('span');
    var spanText = document.createTextNode(name);
    span.appendChild(spanText);
    span.className = 'mdl-checkbox__label';
    label.appendChild(span);
    label.appendChild(input);
    label.className = 'mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect check';
    label.for = layer;
    label.id = layer + 'label';
    componentHandler.upgradeElement(label);
    document.getElementById(id).appendChild(label);
    if (on === true) {
      document.getElementById(layer + 'label').MaterialCheckbox.check();
    }
  }
}
//2.
function addMapBasemapsToggle(id, layer, name, on, val) {
  if (!document.getElementById(layer)) {
    var div = document.createElement('div');
    var label = document.createElement('label');
    var input = document.createElement('input');
    input.id = layer;
    input.type = 'radio';
    input.className = 'mdl-radio__button';
    input.name = "options";
    input.value = val;
    var span = document.createElement('span');
    var spanText = document.createTextNode(name);
    span.appendChild(spanText);
    span.className = 'mdl-checkbox__label';
    label.appendChild(input);
    label.appendChild(span);
    label.id = layer + 'label';
    label.className = 'mdl-radio mdl-js-radio mdl-js-ripple-effect';
    label.for = layer;
    componentHandler.upgradeElement(label);
    div.appendChild(label);
    document.getElementById(id).appendChild(div);
    if (on === true) {
      document.getElementById(layer + 'label').MaterialRadio.check();
    }
  }
}

//Function that adds the parcels(geojson) to the map as a layer and individual features
function addSources() {
  if (LotesData.features) {
    console.log('adding sources');

    /*check for sources first in map*/

    if (!map.getSource('LotesSource')) {
      map.addSource('LotesSource', {
        'type': 'geojson',
        'data': LotesData
      });
      map.addSource('22Out', {
        type: 'raster',
        url: 'mapbox://henriqlima.cjd6rbx1'
      });
      map.addSource('04Out', {
        type: 'raster',
        url: 'mapbox://henriqlima.1x1gmx6l'
      });
      map.addSource('19Set', {
        type: 'raster',
        url: 'mapbox://henriqlima.5vo7e0fy'
      });
      map.addSource('28Ago', {
        type: 'raster',
        url: 'mapbox://henriqlima.8b4xfdnu'
      });
      map.addSource('13Ago', {
        type: 'raster',
        url: 'mapbox://henriqlima.54zeeh62'
      });
      map.addSource('31Jul', {
        type: 'raster',
        url: 'mapbox://henriqlima.8u2xspp1'
      });
      map.addSource('16Jul', {
        type: 'raster',
        url: 'mapbox://henriqlima.04org5ud'
      });
      map.addSource('28Jun', {
        type: 'raster',
        url: 'mapbox://henriqlima.aoazlv96'
      });
      map.addSource('13Jun', {
        type: 'raster',
        url: 'mapbox://henriqlima.2i5j4xug'
      });
      map.addSource('01Jun', {
        type: 'raster',
        url: 'mapbox://henriqlima.38blzxas'
      });
      map.addSource('11Mai', {
        type: 'raster',
        url: 'mapbox://henriqlima.b3nlzvnk'
      });
      map.addSource('29Abr', {
        type: 'raster',
        url: 'mapbox://henriqlima.5lpjgg9j'
      });
      map.addSource('17Abr', {
        type: 'raster',
        url: 'mapbox://henriqlima.au5bupzh'
      });
      map.addSource('30Mar', {
        type: 'raster',
        url: 'mapbox://henriqlima.09kjb72f'
      });
      map.addSource('08Mar', {
        type: 'raster',
        url: 'mapbox://henriqlima.76jdwuqh'
      });
      map.addSource('AutoCad', {
        type: 'raster',
        url: 'mapbox://henriqlima.ak14gdvf'
      });
    }
    addMapLayers()
  } else {
    setTimeout(addSources, 200)
  }
}

function addMapLayers() {
  console.log('adding layers');

  if (!mapLayers.Lotes) {
    ///////MY LAYERS///////
    map.addLayer({
      "id": "Ortho08Mar",
      "type": "raster",
      "source": "08Mar",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho30Mar",
      "type": "raster",
      "source": "30Mar",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho17Abr",
      "type": "raster",
      "source": "17Abr",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho29Abr",
      "type": "raster",
      "source": "29Abr",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho11Mai",
      "type": "raster",
      "source": "11Mai",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho01Jun",
      "type": "raster",
      "source": "01Jun",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho13Jun",
      "type": "raster",
      "source": "13Jun",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho28Jun",
      "type": "raster",
      "source": "28Jun",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho16Jul",
      "type": "raster",
      "source": "16Jul",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho31Jul",
      "type": "raster",
      "source": "31Jul",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho13Ago",
      "type": "raster",
      "source": "13Ago",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho28Ago",
      "type": "raster",
      "source": "28Ago",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho19Set",
      "type": "raster",
      "source": "19Set",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho04Out",
      "type": "raster",
      "source": "04Out",
      'layout': {
        'visibility': 'none'
      }
    });
    map.addLayer({
      "id": "Ortho22Out",
      "type": "raster",
      "source": "22Out"
    });
    map.addLayer({
      "id": "AutoCad",
      "type": "raster",
      "source": "AutoCad",
      'layout': {
        'visibility': 'none'
      }
    });
    /////////////////////////
    mapLayers.Lotes = {
      'id': 'Lotes',
      'type': 'fill',
      'source': 'LotesSource',
      'paint': {
        //'fill-color': 'rgba(0,191,255,0.5)',
        'fill-color': ['match', ['get', 'Situacao'], // get the property Situacao
          'vendido', 'rgba(220,0,0,0.7)', // if 'vendido' then color...
          "disponivel", 'rgba(0,226,44,0.7)', //Verde
          "indisponivel", 'rgba(0,0,0, 0.7)', //Preto
          "reservado socio", 'rgba(255,255,0,0.7)', // ...if 'reservado sócio' then Amarelo
          'rgba(229,229,229, 0.8)'],  // white otherwise
        "fill-outline-color": "slategray"

      },
      'filter': ['!=', 'Lote', ''],
      'layout': {
        'visibility': 'visible'
      }
    };

    mapLayers.LotesLines = {
      'id': 'LotesLines',
      'type': 'line',
      'source': 'LotesSource',
      'paint': {
        'line-width': 1.5,
        "line-color": "slategray"
      },
      'layout': {
        'visibility': 'visible'
      }
    };

    addMapLayersToggle("mapLayerSwitcher", "Lotes", "Lotes", true, 1);
    addMapLayersToggle("mapLayerSwitcher", "AutoCad", "Projeto AutoCAD", false, 2);

    addMapLayersToggle("mapOrtoSwitcher", "Ortho22Out", "22 de Outubro", true, 15);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho04Out", "04 de Outubro", false, 14);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho19Set", "19 de Setembro", false, 13);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho28Ago", "28 de Agosto", false, 12);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho13Ago", "13 de Agosto", false, 11);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho31Jul", "31 de Julho", false, 10);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho16Jul", "16 de Julho", false, 9);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho28Jun", "28 de Junho", false, 8);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho13Jun", "13 de Junho", false, 7);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho01Jun", "01 de Junho", false, 6);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho11Mai", "11 de Maio", false, 5);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho29Abr", "29 de Abril", false, 4);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho17Abr", "17 de Abril", false, 3);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho30Mar", "30 de Março", false, 2);
    addMapLayersToggle("mapOrtoSwitcher", "Ortho08Mar", "08 de Março", false, 1);
  }

  /*
  * add visibility check when changing map styles
  */

  //WORKING ON CODE COMMENTS/ORGANIZING BELOW THIS POINT BECAUSE A COUPLE TESTS ARE IN PROGRESS...

  for (layer in mapLayers) {
    console.log(layer);
    console.log(mapLayers[layer]);
    if (layer === 'Ortho') {
      currentStyle == 'streets-v9' ? map.addLayer(mapLayers[layer], "national_park") :
        map.addLayer(mapLayers[layer], "road-street")
    } else {
      map.addLayer(mapLayers[layer])
    }
  }

  map.on('mouseenter', 'Lotes', function () {
    map.getCanvas().style.cursor = 'pointer';
  });
  map.on('mouseleave', 'Lotes', function () {
    map.getCanvas().style.cursor = '';
  });

  if (baseLayerChange === false) {
    addListeners()
  }
}

function addListeners() {
  console.log('adding event listeners');

  $("#mapLayerSwitcher").on('change', '.mdl-checkbox', function (e) {
    var layer = e.target.id;
    console.log(layer);
    /* switch layers by changing visibility*/
    if (layer != 'checkbox-0') {
      map.getLayoutProperty(layer, 'visibility') === 'none' ?
        map.setLayoutProperty(layer, 'visibility', 'visible') :
        map.setLayoutProperty(layer, 'visibility', 'none');
    }
    map.getLayoutProperty('Lotes', 'visibility') === 'none' ?
      map.setLayoutProperty('LotesLines', 'visibility', 'none') :
      map.setLayoutProperty('LotesLines', 'visibility', 'visible');
    mapLayers.Lotes.layout.visibility = map.getLayoutProperty('Lotes', 'visibility');
    mapLayers.LotesLines.layout.visibility = map.getLayoutProperty('Lotes', 'visibility');
  });

  $("#mapOrtoSwitcher").on('change', '.mdl-checkbox', function (e) {
    var layer = e.target.id;
    console.log(layer);
    /* switch layers by changing visibility*/
    if (layer != 'checkbox-0') {
      map.getLayoutProperty(layer, 'visibility') === 'none' ?
        map.setLayoutProperty(layer, 'visibility', 'visible') :
        map.setLayoutProperty(layer, 'visibility', 'none');
    }
  });

  $(".mdl-radio").change(function () {
    basemapSwitch(this);
  });

  console.log(mapLayers[0]);

  function basemapSwitch(layer) {
    currentStyle = [map.getStyle()];
    var str = currentStyle[0].name;
    str = str.replace(/\s+/g, '-').toLowerCase();
    console.log("current style: " + str);
    newStyle = ($(layer).children()[0].id).toString();
    //var newStyle = layer.target.id;
    console.log("new style: " + newStyle);
    //check if the current style is the same as the clicked style - this means that the style name in the json must match the id of the clicked layer in the material-mapbox template
    if (str != newStyle) {
      //newStyle == 'streets-v9'? mapLayers.Roads.paint["line-color"] = 'black' : mapLayers.Roads.paint["line-color"] = 'white';
      baseLayerChange = true;
      map.setStyle('mapbox://styles/mapbox/' + newStyle);
    }
  }


  map.on('sourcedataloading', function (e) {
    flying = true;
    console.log('loading');
    var checkLoaded = setInterval(loaded, 100);

    function loaded() {
      if (map.getLayer('Lotes')) {
        //console.log('tiles loaded: ', map.areTilesLoaded());
      }
      if (map.areTilesLoaded()) {
        window.clearInterval(checkLoaded);
        console.log('all tiles are loaded', ' flying: ', flying);
        flying = false;
        map.off('sourcedataloading');
      }
    }
  });

  map.on('click', function (e) {
    searchFound = false;
    if (typeof map.getLayer('selected') != "undefined") {
      map.removeLayer('selected')
      // map.removeSource('selected');
    }
    getInfo(e)
  });

  //  map.on('mouseenter', 'Lotes', function(e) {getInfo(e)});

  document.getElementById('map-loading').style.display = 'none';

}

/*
* global getinfo function
*/

function getInfo(e, uid, sr) {
  console.log(uid, sr);
  if (flying === false) {
    if (sr === true) {
      var pt = map.project(e);
      var bbw = 0.001;
      var bbh = 0.001;
      /*var features = map.queryRenderedFeatures(
        [pt.x - bbw, pt.y - bbh],
        [pt.x + bbw, pt.y + bbh], {layers:['Lotes'], filter:['==', 'CPF do Cadastrante', uid]});*/
      var features = map.querySourceFeatures("LotesSource", { sourceLayer: ["Lotes"], filter: ["==", "Lote", uid] });
    } else {
      var features = map.queryRenderedFeatures(e.point, { layers: ['Lotes'] });
    }

    if (!features.length) {
      console.log("these aren't the droids you're looking for")
      if (searchFound === true && tries < 5) {
        setTimeout(function () {
          getInfo(e, uid, sr)
        }, 1000);
      }
      if (searchFound === true && tries === 5) {
        searchFound = false;
        //alert('No features found! If this is unexpected, something went wrong. Please try again.')
      }
      tries = tries + 1;
      return false;
    }

    if (typeof map.getLayer('selected') != "undefined") {
      map.removeLayer('selected')
      //map.removeSource('selected');
    }

    //popup.remove();
    console.log(features);

    //  var uidarray = 

    var feature = features[0];
    //console.log(feature)
    var index = (feature.properties);
    var uid2 = index["Lote"];
    /*  Nome(s) do(s) Proprietário(s)
      CPF(s) do(s) Proprietário(s)
      Nome do Cadastrante
      CPF do Cadastrante
  
      COD_IMOVEL -> Código do Imóvel
      Nome do Imóvel
      Área do Imóvel (ha)
      Módulos Fiscais
      NOM_MUNICI -> Município
      COD_ESTADO -> Estado*/

    var popuploc2 = turf.center(feature);
    console.log(popuploc2.geometry);
    var gmapsloc = popuploc2.geometry.coordinates[1] + "," + popuploc2.geometry.coordinates[0];
    if (userLoc === 0) {
      gmapsorig = "-22.062389,-46.568148"
    }

    var popupTxt = "<strong>Número do Lote: </strong><br>" + index["Lote"] +
      "<br><strong>Situação: </strong><br>" + index["Situacao"] +
      "<br><strong>Valor: </strong><br>" + index["Valor"] +
      "<br><strong>Area: </strong><br>" + index["Area"]

    var popuploc = "";
    if (e.lngLat) {
      popuploc = [e.lngLat.lng, e.lngLat.lat]
    } else {
      popuploc = e;
      /*map.addSource('point', {
        "type": "geojson",
        "data": e
      });

      map.addLayer({
        "id": "selected",
        "type": "circle",
        "source": "point"
      });*/
    }
    /*  popup = new mapboxgl.Popup({
        //closeButton: false
      })
        //.setLngLat([popuploc.geometry.coordinates[0],popuploc.geometry.coordinates[1]])
        .setLngLat(popuploc)
        .setHTML(popupTxt)
        .addTo(map);*/

    /* change to zp-map--info-box--text*/
    $("#map-layer--info-txt").html(popupTxt)
    /*change to zp-map--info-box*/
    $("#mm-info-box").show();
    $(".map-layer--info-close").click(function () {
      map.removeLayer('selected')
      //map.removeSource('selected');
    });

    $("#map-loading").hide();

    /*    map.addSource('selected', {
          "type": "geojson",
          "data": feature.toJSON()
        });*/

    map.addLayer({
      "id": "selected",
      "type": "line",
      "source": "LotesSource",
      "paint": {
        "line-color": "firebrick",
        "line-width": 6
      },
      'filter': ['==', 'Lote', uid2]
    });

    // map.addLayer({
    //   "id": "selected",
    //   "type": "line",
    //   "source": "LotesSource",
    //   "paint": {
    //     "line-color": "Chartreuse",
    //     "line-width": 6
    //   },
    //   'filter':['==','true',vendido]
    // });

    searchFound = false;
  }
}

/*
* search
*/

function searchInit(layer, el) {

  /*initiate search function - see simple jekyll search assets/jsonsearch/search.js*/

  console.log('checking if search json is ready');
  var LotesGeoid = 0;

  LotesSearch = LotesData.features.map(function (f) {
    var centroid = turf.center(f);
    var arr = []
    arr.push(Number(centroid.geometry.coordinates[0]))
    arr.push(Number(centroid.geometry.coordinates[1]))
    f.properties.i = LotesGeoid;
    LotesGeoid = LotesGeoid + 1;
    f.properties.centroid = arr;
    var lotesOk = (f.properties.Lote).toString();
    f.properties.Lote = lotesOk;
    return f.properties
  });

  console.log(LotesCentroids)
  console.log('teste lotesSearch', LotesSearch)

  if (LotesSearch != "") {

    console.log('initiating search');

    console.log(LotesSearch)

    SimpleJekyllSearch({
      searchInput: document.getElementById('map-controls--search-input'),
      resultsContainer: document.getElementById('map-controls--search-results'),
      json: LotesSearch,
      searchResultTemplate: '<a href="#" id="{Lote}" data="{centroid}"> \<li>Lote: {Lote} - Area: {Area}<br>Situação: {Situacao}</li><hr></a>',
      noResultsText: 'Não foi encontrado nenhum resultado!',
      limit: 5,
      fuzzy: false,
      exclude: ['exclude']
    });

    /* results click function */

    $("#map-controls--search-results").on('click', 'a', function (event) {
      tries = 0;
      popup.remove();
      searchFound = true;
      searchResultId = $(this).attr("id");
      searchsearchResultString = searchResultId.toString();
      var xyString = $(this).attr("data");
      var xy = xyString.split(",");
      var point = [Number(xy[0]), Number(xy[1])];
      map.flyTo({ center: point, zoom: 19.49 }); //map.getZoom()
      flying = false;
      var mdlLayout = document.querySelector('.mdl-layout');
      if (document.body.querySelector('.mdl-layout__obfuscator.is-visible')) {
        mdlLayout.MaterialLayout.toggleDrawer();
      }

      var queQueryInterval = setInterval(queQuery, 300);

      function queQuery() {
        if (flying === false) {
          console.log('getting info from search');
          getInfo(point, Number(searchResultId), true);
          clearInterval(queQueryInterval)
        }
      }

      event.preventDefault();
    });
  } else {
    setTimeout(searchInit, 500);
  }
}

/*
* end search function
*/

/* image function save */

$(".mm-modal--map-info-open").click(function () {
  var blobUrl = map.getCanvas().toDataURL();
  var imgcard = $("#mm-modal--map-info .mdl-card");
  var background = "url('" + blobUrl + "')";
  //imgcard.css("background","url('" + blobUrl + "')");
  $("#map-image").html('<img src="data:image/jpg;' + blobUrl + '" width="100%"></img>');
  if (typeof map.getLayer('selected') != "undefined") {
    var selfeature = map.queryRenderedFeatures({ 'layers': ['selected'] });
    var imgname = 'websig_cidades_' + selfeature[0].properties["COD_CADAST"];
    console.log(imgname)
  } else {
    var imgname = 'wbsig_cidades';
  }
  $("#map-image-href").html('<a href="data:image/png;' + blobUrl + '" download="' + imgname + '.png"><i class="material-icons">file_download</i> Download Image</a>')
  $("#mm-modal--map-info").fadeIn();
});

/* end image function */

$(".mm-modal").on('click', '.mm-modal--close', function () {
  $(".mm-modal").fadeOut();
});
$(".mm-modal--open").click(function () {
  $("#mm-modal-info").fadeIn();
});
$(".map-layer--info-close").click(function () {
  $("#mm-info-box").hide();
});
var modalshown = localStorage.getItem('modalshown');
if (modalshown == null) {
  localStorage.setItem('modalshown', 1);
  $("#mm-modal-info").fadeIn("slow");
}
$("#mapLayerInfoClose").click(function () {
  $("#mapLayerInfoWrapper").hide();
});
